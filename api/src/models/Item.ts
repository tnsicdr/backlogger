import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { IItem } from '../interfaces/IItem';

@Entity()
export class Item implements IItem {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column('text')
    name: string;
}
