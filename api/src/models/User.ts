import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Inventory } from './Inventory';
import { IUser } from '../interfaces/IUser';

@Entity()
export class User implements IUser {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column('text')
    username: string;

    @Column('text')
    password: string;

    @OneToOne(() => Inventory, (inventory) => inventory.user)
    inventory: Inventory;

    @Column('datetime')
    createDate: Date;

    @Column('datetime')
    updateDate: Date;
}
