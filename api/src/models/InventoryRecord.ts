import {
    Column,
    Entity,
    JoinColumn,
    ManyToOne,
    OneToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';
import { IInventoryRecord } from '../interfaces/IInventoryRecord';
import { Inventory } from './Inventory';
import { Item } from './Item';

@Entity()
export class InventoryRecord implements IInventoryRecord {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @OneToOne(() => Item)
    @JoinColumn()
    item: Item;

    @Column('decimal')
    quantity: number;

    @Column('decimal')
    incomingQuantity: number;

    @Column('decimal')
    outgoingQuantity: number;

    @Column('decimal')
    activeQuantity: number;

    @ManyToOne(() => Inventory, (inventory) => inventory.records)
    inventory: Inventory;
}
