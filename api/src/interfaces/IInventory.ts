import { IInventoryRecord } from './IInventoryRecord';
import { IUser } from './IUser';

export interface IInventory {
    id: string;
    user: IUser;
    records: Array<IInventoryRecord>;
}
