import { IInventory } from './IInventory';
import { IItem } from './IItem';

export interface IInventoryRecord {
    id: string;
    item: IItem;
    quantity: number;
    incomingQuantity: number;
    outgoingQuantity: number;
    activeQuantity: number;
    inventory: IInventory;
}
