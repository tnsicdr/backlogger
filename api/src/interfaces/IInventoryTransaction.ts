export enum InventoryTransactionDirection {
    VOID = 0,
    INCOMING,
    OUTGOING,
    ACTIVE,
}

export interface IInventoryTransaction {
    itemId: string;
    direction: InventoryTransactionDirection;
    externalReference: string;
    notes: string;
}
