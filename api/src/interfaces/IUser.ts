import { IInventory } from './IInventory';

export interface IUser {
    id: string;
    username: string;
    password: string;
    inventory: IInventory;
    createDate: Date;
    updateDate: Date;
}
